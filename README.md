# LDTO documentation

Documentation of the community related to the Linux Day Torino.

Each section documents a particular area managed by obscure administrators.

The specific contacts are in the bottom of the page.

Thank you for any update in this page!

## LinuxDayTorino.org

The domain linuxdaytorino.org was bought from GoDaddy probably from Roberto Guido "madbob".

The website is hosted by curtesy of Roberto Guido "madbob".

https://linuxdaytorino.org/

The website should be automatically in sync with the upstream repository:

https://gitlab.com/LinuxDayTorino/LinuxDay-Torino-website

Administrators of that website:

- Roberto Guido "madbob"

## ldto.it

The domain ldto.it was bought from OVH from Rosario Antoci (?).

The website hosts a YOURLS instance hosted by curtesy of Valerio Bozzolan "boz":

https://ldto.it/

Manual of YOURLS:

http://yourls.org/

Administrators of that YOURLS (in order of: please contact me):

- Valerio Bozzolan "boz"
- Rosario Antoci "oirasor"

## Blog.LinuxDayTorino.org

The blog website is hosted by curtesy of Valerio Bozzolan "boz":

https://blog.linuxdaytorino.org/

That is a WordPress.

Administrators of that WordPress (in order of: please contact me):

- Valerio Bozzolan "boz"
- Rosario Antoci "oirasor"
- Ludovico Pavesi "lvps"
- Roberto Guido "madbob"

## Matomo.ldto.it

The Matomo website is hosted by curtesy of Italian Linux Society:

https://matomo.ldto.it/

That is just pointing to the very same server handling this website:

https://stats.linux.it/

Administrators of this Matomo (in order of: please contact me):

- Valerio Bozzolan "boz"
- Roberto Guido "madbob"
- Rosario Antoci "oirasor"

## Mobilizon.it

We have a Mobilizon.it account called `@italian_linux_society_torino@mobilizon.it`.

Administrators of that Mobilizon account (in order of: please contact me):

- Valerio Bozzolan "boz"
- Rosario Antoci "oirasor"

## Mastodon.Cisti.org

We have a Mastodon.uno account in Cisti.org that is a Mastodon website from a community of Torino:

https://mastodon.cisti.org/@LinuxDayTorino

We have not any direct friend in Cisti.org. We are just there to try that installation, since they are from Torino. It works, btw.

Administrators of that Mastodon account (in order of: please contact me):

- Valerio Bozzolan "boz"
- Rosario Antoci "oirasor"
- Roberto Guido "madbob"

## Telegram group @linuxtorino

We have a partially-related Telegram account called `@linuxtorino`. The group is public.

Administrators of that Telegram group:

* just contact an already-existing member of that group to see the up-to-date list of Administrators.

## Email @ldto.it

The mailbox info at ldto.it is curtesy of Valerio Bozzolan.

Mailbox panel administrators that can reset the password etc. (in order of: please contact me):

- Valerio Bozzolan "boz"
- Rosario Antoci "oirasor"

Postmaster:

- Valerio Bozzolan "boz"

Documentation of IMAP and SMTP parameters:

https://mail.reyboz.it/

## Persons and contacts

(In alphabetical order)

- Ludovico Pavesi "lvps"
- Roberto Guido "madbob" - email: bob at linux dot it - Fediverse: https://sociale.network/@madbob
- Rosario Antoci "oirasor" - Fediverse: https://mastodon.uno/@oirasor
- Valerio Bozzolan "boz" -  gnu at linux dot it - Fediverse: https://mastodon.uno/@boz

## License

This page is just a bunch of information, not creative in any way. But, in case, any contribution is under the public domain.

https://creativecommons.org/publicdomain/zero/1.0/
